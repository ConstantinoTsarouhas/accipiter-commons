# The Accipiter Commons repository
**Accipiter Commons** is a repository of interdependent, open-source, Swift-based libraries that are considered an extension of the Swift standard library and have a common set of design goals.

+ An Accipiter Commons library is **agnostic** of its software environment. It must transparently support all targets supported by the standard Swift build system and minimise the surface of platform-specific API (and, if possible, have no such API at all).  
For example, *Safe* provides platform-agnostic API that operates on document files regardless of the underlying filesystem. File coordination and ubiquitous storage management, both required document-related features on iOS and OS X, are hidden and dealt with within the library's implementation and away from public API.

+ An Accipiter Commons library is **concise** in its feature set and its API. The library's functionality should be describable in a sentence.  
For example, *Production* is a library for *parsing strings*. Production does not contain a string generator (the inverse operation of parsing), unrelated collection types, or an embedded game.

+ An Accipiter Commons library is as **general** as is feasable. Every assumption such as the kind of string or whether input is immediately available (instead of possibly being on a remote device) reduces the reach of a library, which is most undesirable for a library that is considered an extension to the standard library.  
For example, the *Production* library performs matching & parsing on equatable collections—`String.CharacterView` is one such collection but `[UInt8]` byte arrays are equally suitable as is any developer-defined equatable collection.

+ An Accipiter Commons library feels **native** to the Swift corpus. The library should follow all of the language's API guidelines and make extensive use of standard library types and functions.

![Accipiter Commons libraries and their interdependencies](graph.png "Accipiter Commons libraries and their interdependencies")

Accipiter Commons is currently a work in progress.